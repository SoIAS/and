package com.wdowiak.temp_conversion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class TempConversionActivity extends AppCompatActivity
{
    private EditText edit_celsius, edit_fahrenheit, edit_kelvin;

    enum temperature_type
    {
        celsius,
        fahrenheit,
        kelvin
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_celsius = findViewById(R.id.edit_celsius);
        edit_fahrenheit = findViewById(R.id.edit_fahrenheit);
        edit_kelvin = findViewById(R.id.edit_kelvin);

        edit_celsius.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent)
            {
                if(keyEvent.getAction() == KeyEvent.ACTION_UP)
                {
                    final String text_value = edit_celsius.getText().toString();

                    if(text_value.isEmpty())
                    {
                        clearAllBut(temperature_type.celsius);
                        return true;
                    }

                    try
                    {
                        final double value = Double.valueOf(text_value);
                        setFahrenheit(value, temperature_type.celsius);
                        setKelvin(value, temperature_type.celsius);
                    }
                    catch (NumberFormatException e)
                    {
                        clearAllBut(temperature_type.celsius);
                    }

                    return true;
                }

                return false;
            }
        });

        edit_fahrenheit.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent)
            {
                if(keyEvent.getAction() == KeyEvent.ACTION_UP)
                {
                    final String text_value = edit_fahrenheit.getText().toString();

                    if(text_value.isEmpty())
                    {
                        clearAllBut(temperature_type.fahrenheit);
                        return true;
                    }

                    try
                    {
                        final double value = Double.valueOf(text_value);
                        setCelsius(value, temperature_type.fahrenheit);
                        setKelvin(value, temperature_type.fahrenheit);
                    }
                    catch (NumberFormatException e)
                    {
                        clearAllBut(temperature_type.fahrenheit);
                    }

                    return true;
                }

                return false;
            }
        });

        edit_kelvin.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent)
            {
                if(keyEvent.getAction() == KeyEvent.ACTION_UP)
                {
                    final String text_value = edit_kelvin.getText().toString();

                    if(text_value.isEmpty())
                    {
                        clearAllBut(temperature_type.kelvin);
                        return true;
                    }

                    try
                    {
                        final double value = Double.valueOf(text_value);
                        setCelsius(value, temperature_type.kelvin);
                        setFahrenheit(value, temperature_type.kelvin);
                    }
                    catch(NumberFormatException e)
                    {
                        clearAllBut(temperature_type.kelvin);
                    }

                    return true;
                }

                return false;
            }
        });
    }

    private final void clearAllBut(final temperature_type exclude_from_clear)
    {

        switch(exclude_from_clear)
        {
            case celsius:
                edit_fahrenheit.setText(null);
                edit_kelvin.setText(null);
                break;

            case fahrenheit:
                edit_celsius.setText(null);
                edit_kelvin.setText(null);
                break;

            case kelvin:
                edit_celsius.setText(null);
                edit_fahrenheit.setText(null);
                break;

            default:
                throw new RuntimeException("Unexpected temperature type");
        }
    }


    private final void setCelsius(final double value, final temperature_type source_type)
    {
        final double conversed_value = toCelsius(value, source_type);
        edit_celsius.setText(Double.toString(conversed_value));
    }

    private final void setFahrenheit(final double value, final temperature_type source_type)
    {
        final double conversed_value = celsiusTo(toCelsius(value, source_type), temperature_type.fahrenheit);
        edit_fahrenheit.setText(Double.toString(conversed_value));
    }

    private final void setKelvin(final double value, final temperature_type source_type)
    {
        final double conversed_value = celsiusTo(toCelsius(value, source_type), temperature_type.kelvin);
        edit_kelvin.setText(Double.toString(conversed_value));
    }

    private final double toCelsius(final double value, final temperature_type source_type)
    {
        switch(source_type)
        {
            case celsius:
                return value;

            case fahrenheit:
                return (value - 32) * 5.0 / 9.0;

            case kelvin:
                return value - 273.15;

            default:
                throw new RuntimeException("Unexpected temperature type");
        }
    }

    private final double celsiusTo(final double value, final temperature_type destination_type)
    {
        switch(destination_type)
        {
            case celsius:
                return value;

            case fahrenheit:
                return (value * 9.0 / 5.0) + 32;

            case kelvin:
                return value + 273.15;

            default:
                throw new RuntimeException("Unexpected temperature type");
        }
    }
}
