package pl.damiansumara.listazadan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by Dawid Wdowiak on 1/15/2018.
 */

public class ImageAttachment extends BaseAttachment
{
    private Bitmap bitmap;

    ImageAttachment(int id, EAttachmentType attachmentType, Bitmap bitmap)
    {
        this.id = id;
        this.attachmentType = attachmentType;
        this.bitmap = bitmap;
    }

    ImageAttachment(int id, EAttachmentType attachmentType, byte[] data)
    {
        this.id = id;
        this.attachmentType = attachmentType;
        this.bitmap = convertDataToBitmap(data);
    }

    ImageAttachment(EAttachmentType attachmentType, Bitmap bitmap)
    {
        this.attachmentType = attachmentType;
        this.bitmap = bitmap;
    }

    ImageAttachment(EAttachmentType attachmentType, byte[] data)
    {
        this.attachmentType = attachmentType;
        this.bitmap = convertDataToBitmap(data);
    }

    public byte[] getImageAsDataArray()
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public Bitmap getImageAsBitmap()
    {
        return bitmap;
    }


    private Bitmap convertDataToBitmap(byte[] data)
    {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }


}
