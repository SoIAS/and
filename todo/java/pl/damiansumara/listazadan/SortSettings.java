package pl.damiansumara.listazadan;

/**
 * Created by Dawid Wdowiak on 1/12/2018.
 */

enum EFilterType
{
    NONE,
    NOT_COMPLETED,
    COMPLETED,
    OVERDUE,
    DUE_IN_24H,
    DUE_TODAY,
    DUE_IN_7D,
    DUE_IN_30D
}

enum ESortOrder
{
    ASCENDING,
    DESCENDING
};

enum ESortBy
{
    NAME,
    PRIORITY,
    COMPLETED,
    CREATION_DATE,
    DUE_DATE
}

public class SortSettings
{
    public static ESortOrder DEFAULT_ORDER = ESortOrder.ASCENDING;

    private ESortBy sortBy;
    private ESortOrder sortOrder;

    SortSettings()
    {
        this.sortBy = ESortBy.NAME;
        this.sortOrder = DEFAULT_ORDER;
    }

    SortSettings(ESortBy sortBy, ESortOrder sortOrder)
    {
        this.sortBy = sortBy;
        this.sortOrder = DEFAULT_ORDER = sortOrder;
    }

    public ESortBy getSortBy()
    {
        return sortBy;
    }

    public ESortOrder getSortOrder()
    {
        return sortOrder;
    }

    // Change sort type to the other
    public void changeSortOrder()
    {
        sortOrder = sortOrder == ESortOrder.ASCENDING ? ESortOrder.DESCENDING : ESortOrder.ASCENDING;
    }

    public void changeSortBy(ESortBy sortBy)
    {
        if(this.sortBy == sortBy)
        {
            // just change direction
            changeSortOrder();
            return;
        }

        this.sortBy = sortBy;
        this.sortOrder = DEFAULT_ORDER;
    }
}
