package pl.damiansumara.listazadan;

/**
 * Created by Dawid Wdowiak on 1/16/2018.
 */

enum EAttachmentType
{
    IMAGE,
    VIDEO
}

public class BaseAttachment
{
    protected int id;
    protected EAttachmentType attachmentType;

    public int getId()
    {
        return id;
    }

    public EAttachmentType getAttachmentType()
    {
        return attachmentType;
    }
}
