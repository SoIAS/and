package pl.damiansumara.listazadan;

import android.Manifest;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class TaskDetailActivity extends AppCompatActivity
{
    // todo, classwide dbhelper, since we are using it a lot here
    public static final String EXTRA_TASK_ID = "TASK_ID";
    public static final int RESULT_ADD_IMAGE = 1;
    public static final int RESULT_ADD_IMAGE_CAMERA = 2;
    public static final int RESULT_RECORD_VIDEO = 3;
    public static final int RESULT_ADD_VIDEO = 4;

    public static final int PERMISSION_REQUEST_SHOW_ATTACHMENTS = 1;
    public static final int PERMISSION_REQUEST_ADD_IMAGE_FROM_STOARGE = 2;
    public static final int PERMISSION_REQUEST_ADD_VIDEO_FROM_STOARGE = 3;
    public static final int PERMISSION_REQUEST_ADD_IMAGE_FROM_CAMERA = 4;
    public static final int PERMISSION_REQUEST_RECORD_VIDEO = 5;

    int taskId;

    TextView taskName;
    TextView taskPriority;
    TextView taskCreationDate;
    TextView taskDueDate;
    TextView taskCompletionDate;
    CheckBox taskCompleted;
    GridView attachmentGridView;

    TextView dueLabel, completedLabel;

    AttachmentAdapter attachmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);

        taskId = getIntent().getIntExtra(EXTRA_TASK_ID, -1);

        taskName = findViewById(R.id.detail_taskname);
        taskPriority = findViewById(R.id.detail_taskPriority);
        taskCreationDate = findViewById(R.id.detail_creationDate);
        taskDueDate = findViewById(R.id.detail_dueDate);
        taskCompletionDate = findViewById(R.id.detail_completionDate);
        taskCompleted = findViewById(R.id.detail_taskComplete);
        attachmentGridView = findViewById(R.id.detail_gridView);

        dueLabel = findViewById(R.id.detailLabel_dueLabel);
        completedLabel = findViewById(R.id.detailLabel_completedLabel);

        taskCompleted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                DbHelper dbHelper = new DbHelper(getApplicationContext());
                dbHelper.setTaskStatus(taskId, isChecked);

                final ToDoTask task = dbHelper.getTask(taskId);

                final int completionVisibility = task.getDone() ? View.VISIBLE : View.GONE;
                final int dueVisibility = task.getDone() ? View.GONE : View.VISIBLE;

                taskCompletionDate.setText(Helpers.dateToString(task.getCompletionDate()));
                taskCompletionDate.setVisibility(completionVisibility);
                completedLabel.setVisibility(completionVisibility);

                taskDueDate.setVisibility(dueVisibility);
                dueLabel.setVisibility(dueVisibility);

                dbHelper.close();
            }
        });

        attachmentGridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                final BaseAttachment attachment = attachmentAdapter.getItem(position);

                Intent intent = null;
                switch(attachment.getAttachmentType())
                {
                    case IMAGE:
                        intent = new Intent(getApplicationContext(), ImageAttachmentDetailActivity.class);
                        intent.putExtra(ImageAttachmentDetailActivity.ARGUMENT_IMAGE_ATTACHMENT_ID, attachment.getId());

                        break;

                    case VIDEO:
                        intent = new Intent(getApplicationContext(), VideoAttachmentDetailActivity.class);
                        intent.putExtra(VideoAttachmentDetailActivity.ARGUMENT_VIDEO_ATTACHMENT_ID, attachment.getId());

                        break;
                }

                startActivity(intent);
            }
        });

        openTask();
        showAttachmentsRequestPermission();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        openTask();
    }

    // So backstack will work when opening from notification
    @Override
    public void onBackPressed()
    {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.detailmenu_edit:
            {
                Intent intent = new Intent(getApplicationContext(), AddEditTaskActivity.class);
                intent.putExtra(AddEditTaskActivity.ARGUMENT_EDIT_TASK_ID, taskId);
                startActivity(intent);
                break;
            }

            case R.id.detailmenu_delete:
            {
                deleteTask();

                break;
            }

            case R.id.detailmenu_addImage:
            {
                addImageFromStorageRequestPermission();
                break;
            }

            case R.id.detailmenu_addImageCamera:
            {
                addImageFromCameraRequestPermission();
                break;
            }

            case R.id.detailmenu_addVideo:
            {
                addVideoFromStorageRequestPermission();
                break;
            }

            case R.id.detailmenu_recordVideo:
            {
                recordVideoRequestPermission();
                break;
            }

            case R.id.detailmenu_removeAllAttachments:
            {
                ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
                    @Override
                    public void onConfirmation() {
                        DbHelper dbHelper = new DbHelper(getApplicationContext());
                        dbHelper.deleteAllImagesFromTask(taskId);
                        dbHelper.deleteAllVideosFromTask(taskId);
                        dbHelper.close();

                        loadAttachments();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

                dialog.setConfirmationText("Are you sure you want to remove ALL attachments?");
                dialog.show();

                break;
            }

        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && data != null)
        {
            switch(requestCode)
            {
                case RESULT_ADD_IMAGE:
                    Uri selectedImage = data.getData();

                    try
                    {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                        ImageAttachment imageAttachment = new ImageAttachment(EAttachmentType.IMAGE, bitmap);

                        DbHelper dbHelper = new DbHelper(getApplicationContext());
                        dbHelper.addImageToTask(taskId, imageAttachment);
                        dbHelper.close();

                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                    break;

                case RESULT_ADD_IMAGE_CAMERA:
                {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    ImageAttachment imageAttachment = new ImageAttachment(EAttachmentType.IMAGE, bitmap);

                    DbHelper dbHelper = new DbHelper(getApplicationContext());
                    dbHelper.addImageToTask(taskId, imageAttachment);
                    dbHelper.close();
                    break;
                }
                case RESULT_ADD_VIDEO:
                    // passthough
                case RESULT_RECORD_VIDEO:
                    Uri videoUri = data.getData();

                    // todo
                    String[] proj = { MediaStore.Images.Media.DATA };
                    CursorLoader loader = new CursorLoader(getApplicationContext(), videoUri, proj, null, null, null);
                    Cursor cursor = loader.loadInBackground();
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    String result = cursor.getString(column_index);
                    cursor.close();



                    DbHelper dbHelper = new DbHelper(getApplicationContext());
                    dbHelper.addVideoToTask(taskId, new VideoAttachment(EAttachmentType.VIDEO, result));
                    dbHelper.close();
                    break;


            }
        }

        loadAttachments();

    }

    protected void openTask()
    {
        DbHelper dbHelper = new DbHelper(getApplicationContext());
        ToDoTask task = dbHelper.getTask(taskId);
        dbHelper.close();

        if(task == null)
        {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            AlertDialog alert = new AlertDialog.Builder(this)
                    .setMessage("Task does not exist anymore")
                    .setTitle("Task not found")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    }).create();

            alert.show();
        }


        showTask(task);
    }

    protected void deleteTask()
    {
        ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
            @Override
            public void onConfirmation()
            {
                DbHelper dbHelper = new DbHelper(getApplicationContext());
                dbHelper.deleteTask(taskId);
                dbHelper.close();

                // Detail activity is only called by MainActivity, so we can simply finish() here and go back to main
                finish();
            }

            @Override
            public void onCancel()
            {

            }
        });

        dialog.setConfirmationText("Are you sure you want to remove this task?");
        dialog.show();
    }

    protected void showTask(ToDoTask task)
    {
        if(task == null)
        {
            return;
        }

        final int completionVisibility = task.getDone() ? View.VISIBLE : View.GONE;
        final int dueVisibility = task.getDone() ? View.GONE : View.VISIBLE;

        taskName.setText(task.getName());
        taskPriority.setText(task.getPriorityString(getApplicationContext()));
        taskCreationDate.setText(Helpers.dateToString(task.getCreationDate()));
        taskDueDate.setText(task.hasDueDate() ? Helpers.dateToString(task.getDueDate()) : "No due date");
        taskDueDate.setVisibility(dueVisibility);
        taskCompletionDate.setText(Helpers.dateToString(task.getCompletionDate()));
        taskCompletionDate.setVisibility(completionVisibility);
        taskCompleted.setChecked(task.getDone());

        completedLabel.setVisibility(completionVisibility);
        dueLabel.setVisibility(dueVisibility);
    }

    protected void loadAttachments()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        DbHelper dbHelper = new DbHelper(getApplicationContext());
        showAttachments(dbHelper.getAttachmentsFromTask(taskId));
        dbHelper.close();
    }

    protected void showAttachments(ArrayList<BaseAttachment> attachments)
    {
        attachmentGridView.setVisibility(View.VISIBLE);
        if (attachmentAdapter == null)
        {
            attachmentAdapter = new AttachmentAdapter(attachments, getApplicationContext());
            attachmentGridView.setAdapter(attachmentAdapter);
        }
        else
        {
            attachmentAdapter.clear();
            attachmentAdapter.addAll(attachments);
            attachmentAdapter.notifyDataSetChanged();
        }
    }

    protected void hideAttachments()
    {
        attachmentGridView.setVisibility(View.GONE);
        attachmentAdapter = null;
    }

    public void deleteAttachment(final View view)
    {
        ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
            @Override
            public void onConfirmation()
            {
                final int attachmentPosition = (int)view.getTag();
                final BaseAttachment attachment = attachmentAdapter.getItem(attachmentPosition);

                DbHelper dbHelper = new DbHelper(getApplicationContext());
                switch(attachment.getAttachmentType())
                {
                    case IMAGE:
                        dbHelper.deleteImageFromTask(attachment.getId());
                        break;

                    case VIDEO:
                        dbHelper.deleteVideoFromTask(attachment.getId());
                        break;
                }
                dbHelper.close();

                loadAttachments();
            }

            @Override
            public void onCancel()
            {

            }
        });

        dialog.setConfirmationText("Are you sure you want to remove this attachment?");
        dialog.show();
    }

    private void beginAddImageFromStorageIntent()
    {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_ADD_IMAGE);
    }

    private void beginAddImageFromCameraIntent()
    {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESULT_ADD_IMAGE_CAMERA);
    }

    public void beginAddVideoFromStorageIntent()
    {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_ADD_VIDEO);
    }

    public void beginRecordVideoIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, RESULT_RECORD_VIDEO);
    }

    private void showAttachmentsRequestPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_SHOW_ATTACHMENTS);
        }
        else
        {
            loadAttachments();
        }
    }

    private void addImageFromStorageRequestPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_ADD_IMAGE_FROM_STOARGE);
        }
        else
        {
            beginAddImageFromStorageIntent();
        }
    }

    private void addImageFromCameraRequestPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_ADD_IMAGE_FROM_CAMERA);
        }
        else
        {
            beginAddImageFromCameraIntent();
        }
    }

    private void addVideoFromStorageRequestPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_ADD_VIDEO_FROM_STOARGE);
        }
        else
        {
            beginAddVideoFromStorageIntent();
        }
    }
    private void recordVideoRequestPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_RECORD_VIDEO);
        }
        else
        {
            beginRecordVideoIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int grantResults[])
    {
        switch (requestCode)
        {
            case PERMISSION_REQUEST_SHOW_ATTACHMENTS:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    loadAttachments();
                }
                else
                {
                    hideAttachments();
                    Toast.makeText(this, "Attachments won't be displayed without permission", Toast.LENGTH_LONG).show();
                }
                break;

            case PERMISSION_REQUEST_ADD_IMAGE_FROM_STOARGE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    beginAddImageFromStorageIntent();
                    loadAttachments();
                }
                else
                {
                    Toast.makeText(this, "Cannot add image from storage without permission", Toast.LENGTH_LONG).show();
                }
                break;

            case PERMISSION_REQUEST_ADD_IMAGE_FROM_CAMERA:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    beginAddImageFromCameraIntent();
                    loadAttachments();
                }
                else
                {
                    Toast.makeText(this, "Cannot add image from camera without permission", Toast.LENGTH_LONG).show();
                }
                break;

            case PERMISSION_REQUEST_ADD_VIDEO_FROM_STOARGE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    beginAddVideoFromStorageIntent();
                    loadAttachments();
                }
                else
                {
                    Toast.makeText(this, "Cannot add video from storage without permission", Toast.LENGTH_LONG).show();
                }
                break;

            case PERMISSION_REQUEST_RECORD_VIDEO:
                if(grantResults.length == 3)
                {
                    final boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    final boolean recordAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    final boolean readExternalStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if(cameraPermission && recordAudioPermission && readExternalStoragePermission)
                    {
                        beginRecordVideoIntent();
                        loadAttachments();
                    }
                    else
                    {
                        Toast.makeText(this, "Cannot record video without permission", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toast.makeText(this, "Cannot record video without permission", Toast.LENGTH_LONG).show();
                }

        }
    }
}
