package pl.damiansumara.listazadan;

import android.content.Context;

import java.util.Date;

public class ToDoTask {

    private Integer id;
    private String name;
    private Integer priority;
    private Boolean done;
    private Date creation_date;
    private Date due_date;
    private Date completion_date;

    public ToDoTask(String name, Integer priority, Date due_date) {
        this.name = name;
        this.priority = priority;
        this.done = false;
        this.due_date = due_date;
    }

    public ToDoTask(Integer id, String name, Integer priority, Boolean done, Date creation_date, Date due_date, Date comletion_date) {
        this.id = id;
        this.name = name;
        this.priority = priority;
        this.done = done;
        this.creation_date = creation_date;
        this.due_date = due_date;
        this.completion_date = comletion_date;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getPriorityString(Context context)
    {
       return context.getResources().getStringArray(R.array.priority_strings)[priority];
    }

    public Boolean getDone() {
        return done;
    }

    public Date getCreationDate() {
        return creation_date;
    }

    public Date getDueDate()
    {
        return due_date;
    }

    public boolean hasDueDate() { return due_date.getTime() > 0; }

    public Date getCompletionDate() {return completion_date;}

    // Context needed for the priority string array
    public String toString(Context context)
    {
        final String completedString = context.getResources().getString(R.string.completed);
        final String notCompletedString = context.getResources().getString(R.string.not_completed);
        final String noneString = context.getResources().getString(R.string.none);

        String str = "Name: " + name +
                "\nPriority: " + getPriorityString(context)+
                "\nCreation Date: " + creation_date.toString() +
                (done ? ("\nCompletion date: " + completion_date.toString()) :
                        ("\nDue Date: " + (hasDueDate() ? due_date.toString() : noneString))) +
                "\nStatus: " + (done ? completedString : notCompletedString);
        return str;
    }

//     compare object for remove from array list
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj)
//            return true;
//        if (obj == null)
//            return false;
//        if (getClass() != obj.getClass())
//            return false;
//        ToDoTask other = (ToDoTask) obj;
//        if (!content.equals(other.content))
//            return false;
//        if (!reminderDate.equals(other.reminderDate))
//            return false;
//        if (hasReminder != other.hasReminder)
//            return false;
//        if (done != other.done)
//            return false;
//        return true;
//    }
}
