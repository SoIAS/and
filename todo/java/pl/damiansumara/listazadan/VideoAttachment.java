package pl.damiansumara.listazadan;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * Created by Dawid Wdowiak on 1/16/2018.
 */

public class VideoAttachment extends BaseAttachment
{
    String filePath;
    Bitmap thumbnail;

    VideoAttachment(EAttachmentType attachmentType, String filePath)
    {
        this.attachmentType = attachmentType;
        this.filePath = filePath;
    }

    VideoAttachment(int id, EAttachmentType attachmentType, String filePath)
    {
        this.id = id;
        this.attachmentType = attachmentType;
        this.filePath = filePath; // todo, assert vidoe exists
        this.thumbnail = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND); // or maybe put the thumbnail into database?
    }

    public String getVideoFilepath()
    {
        return filePath;
    }

    public Bitmap getThumbnail()
    {
        return thumbnail;
    }

    public Uri getVideoUri()
    {
        return Uri.parse(filePath);
    }

}
