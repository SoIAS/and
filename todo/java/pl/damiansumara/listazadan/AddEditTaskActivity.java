package pl.damiansumara.listazadan;

import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class AddEditTaskActivity extends AppCompatActivity
{
    public static final String ARGUMENT_EDIT_TASK_ID = "EDIT_TASK_ID";

    EditText taskName;
    Spinner taskPriority;
    TextView taskDueDate, nameValid;
    int taskId;

    Date dueDate; // Either set by populateTask, datePickerFragment, or timePickerFragment

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addedit_task);

        taskName = findViewById(R.id.addtask_name);
        taskPriority = findViewById(R.id.addtask_priority);
        taskDueDate = findViewById(R.id.addtask_dueDate);

        nameValid = findViewById(R.id.addtask_nameValid);

        dueDate = new Date(0);

        taskId = getIntent().getIntExtra(ARGUMENT_EDIT_TASK_ID, -1);
        if(!isNewTask())
        {
            populateTask();
        }

        findViewById(R.id.addtask_datePickerButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                DialogFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getFragmentManager().beginTransaction(), "DatePickerFragment");
            }
        });

        findViewById(R.id.addtask_timePickerButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                DialogFragment timePickerFragment = new TimePickerFragment();
                timePickerFragment.show(getFragmentManager().beginTransaction(), "TimePickerFragment");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addedit_menu, menu);

        changeMenuTitles(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.addeditmenu_confirm:
            {
                if( taskName.getText().toString().isEmpty() )
                {
                    nameValid.setVisibility(View.VISIBLE);
                    nameValid.setText(R.string.name_value);
                    break;
                }

                if(isNewTask())
                {
                    addNewTask();
                }
                else
                {
                    editExistingTask();
                }
                break;
            }
        }

        return true;
    }

    protected void editExistingTask()
    {
        ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
            @Override
            public void onConfirmation()
            {
                DbHelper dbHelper = new DbHelper(getApplicationContext());
                dbHelper.updateTask(taskId, taskName.getText().toString(), taskPriority.getSelectedItemPosition(), dueDate);
                dbHelper.close();

                finish();
            }

            @Override
            public void onCancel()
            {
                Toast.makeText(AddEditTaskActivity.this, "Saves were not changed", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.setConfirmationText("Are you sure you want to save changes?");
        dialog.show();
    }

    protected void addNewTask()
    {
        ToDoTask task = new ToDoTask(taskName.getText().toString(), taskPriority.getSelectedItemPosition(), dueDate);

        DbHelper dbHelper = new DbHelper(getApplicationContext());
        dbHelper.newTask(task);
        dbHelper.close();

        finish();
    }

    protected boolean isNewTask()
    {
        return taskId < 0;
    }

    protected void populateTask()
    {
        DbHelper dbHelper = new DbHelper(getApplicationContext());
        ToDoTask task = dbHelper.getTask(taskId);

        if(task != null)
        {
            taskName.setText(task.getName());
            taskPriority.setSelection(task.getPriority());

            dueDate = task.getDueDate();
            setDueDueText();

        }

        dbHelper.close();
    }

    protected void changeMenuTitles(Menu menu)
    {
        MenuItem confirmItem = menu.findItem(R.id.addeditmenu_confirm);
        confirmItem.setTitle(isNewTask() ? R.string.add_task : R.string.save_changes);
    }

    // TODO, maybe use better solution for the date and time, but for now it works
    public void setDueDate(Date dueDate)
    {
        final long dueTime = getDueTimeFromDate(this.dueDate);

        // add the time to date
        this.dueDate.setTime(dueDate.getTime() + dueTime);

        setDueDueText();
    }

    public void setDueTime(long dueTimeInMs)
    {
        if(this.dueDate.getTime() == 0)
        {
            // if no date was set, but user has set time, use todays date
            this.dueDate.setTime(System.currentTimeMillis());
        }

        // Remove old time from date
        final long dueDate = this.dueDate.getTime() - getDueTimeFromDate(this.dueDate);
        this.dueDate.setTime(dueDate + dueTimeInMs);

        setDueDueText();
    }

    /* returns time of the day in ms from date */
    public long getDueTimeFromDate(Date date)
    {
        final long millisecondsInADay = 24 * 60 * 60 * 1000;
        final long millisecondsInAnHour = 60 * 60 * 1000;
        return date.getTime() % millisecondsInADay + (date.getTime() == 0 ? 0 : millisecondsInAnHour);
    }

    public void clearDueDate(final View view)
    {
        this.dueDate.setTime(0);
        setDueDueText();
    }

    private void setDueDueText()
    {
        taskDueDate.setText(dueDate.getTime() > 0 ? Helpers.dateToString(dueDate) : "");
    }
}
