package pl.damiansumara.listazadan;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dawid Wdowiak on 1/17/2018.
 */

public class Helpers
{
    static SimpleDateFormat getSimpleDateFormat()
    {
        return new SimpleDateFormat("yyyy-MM-dd kk:mm");
    }

    static String dateToString(Date date)
    {
        return getSimpleDateFormat().format(date);
    }
}
