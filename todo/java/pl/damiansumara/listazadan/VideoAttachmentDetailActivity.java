package pl.damiansumara.listazadan;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;

/**
 * Created by Dawid Wdowiak on 1/16/2018.
 */

public class VideoAttachmentDetailActivity extends Activity
{
    public static final String ARGUMENT_VIDEO_ATTACHMENT_ID = "VIDEO_ATTACHMENT_ID";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_attachment_detail);

        final int attachmentId = getIntent().getIntExtra(ARGUMENT_VIDEO_ATTACHMENT_ID, -1);

        DbHelper dbHelper = new DbHelper(getApplicationContext());
        final VideoAttachment videoAttachment = dbHelper.getVideoFromTask(attachmentId);
        dbHelper.close();

        if (attachmentId < 0 || videoAttachment == null)
        {
            finish();
        }


        // Doesnt work when we setFromUri...
        File f = new File(videoAttachment.getVideoFilepath());

        VideoView videoView = findViewById(R.id.attachmentDetail_videoView);
        videoView.setMediaController(new MediaController(this));
        videoView.setVideoURI(Uri.fromFile(f));
        videoView.start();
    }
}