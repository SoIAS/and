package com.wdowiak.avgfuelcombustion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    EditText editFuel, editDistnace;
    TextView textResult;
    Spinner spinnerFuel, spinnerDistance;

    String fuelVolumeTypes[] = null;
    String distanceTypes[] = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fuelVolumeTypes = getResources().getStringArray(R.array.fuel_volume_types);
        distanceTypes = getResources().getStringArray(R.array.distance_types);

        editFuel = findViewById(R.id.edit_fuel);
        editDistnace = findViewById(R.id.edit_distance);
        textResult = findViewById(R.id.text_result);

        spinnerFuel = findViewById(R.id.spinner_fuel);
        spinnerDistance = findViewById(R.id.spinner_distance);

        spinnerFuel.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, fuelVolumeTypes));
        spinnerDistance.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, distanceTypes));

        findViewById(R.id.btn_calculate).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    final double fuel = Double.valueOf(editFuel.getText().toString());
                    final double distnace = Double.valueOf(editDistnace.getText().toString());

                    if(fuel > 0 && distnace > 0)
                    {
                        final double average_fuel_combustion = (fuel/distnace)*100;
                        DecimalFormat formatter = new DecimalFormat("0.##");

                        textResult.setText("Average fuel combustion is " + formatter.format(average_fuel_combustion) + " " + concatenateResultTypes());
                    }
                    else
                    {
                        textResult.setText("Cennot calculate average fuel combustion if either of these variables are 0 or not set");
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private final String concatenateResultTypes()
    {
        return spinnerFuel.getSelectedItem().toString().toLowerCase() + " per 100 " + spinnerDistance.getSelectedItem().toString().toLowerCase();
    }

}
