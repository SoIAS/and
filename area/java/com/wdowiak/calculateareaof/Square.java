package com.wdowiak.calculateareaof;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Square extends AppCompatActivity {

    TextView tv_area_result, tv_circ_result;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square);

        tv_area_result = findViewById(R.id.tv_square_area);
        tv_circ_result = findViewById(R.id.tv_square_circ);

        findViewById(R.id.square_calculate_btn).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final EditText edit_side_len = findViewById(R.id.square_side_length);

                tv_area_result.setText(null);
                tv_circ_result.setText(null);

                try
                {
                    final double side_lenght = Double.valueOf(edit_side_len.getText().toString());

                    final double area = side_lenght * side_lenght;
                    final double circ = side_lenght * 4;

                    tv_area_result.setText("Area of the square equals " + area);
                    tv_circ_result.setText("Circumference of the square equals " + circ);
                }
                catch (NumberFormatException e)
                {
                    tv_area_result.setText("Cannot parse the value of side length to double");
                    e.printStackTrace();
                }
            }
        });
    }
}
