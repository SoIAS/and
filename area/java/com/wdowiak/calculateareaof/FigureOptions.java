package com.wdowiak.calculateareaof;

public class FigureOptions
{
    FigureOptions(String name, Class<?> cls)
    {
        this.name = name;
        this.cls = cls;
    }

    final String getName()
    {
        return name;
    }

    final Class<?> getCls()
    {
        return cls;
    }

    String name;
    Class<?> cls;
}
