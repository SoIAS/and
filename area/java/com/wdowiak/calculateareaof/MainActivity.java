package com.wdowiak.calculateareaof;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    static ArrayList<FigureOptions> figureOptions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        figureOptions.add(new FigureOptions("Square", Square.class));
        figureOptions.add(new FigureOptions("Triangle", Square.class));
        figureOptions.add(new FigureOptions("Circle", Square.class));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = findViewById(R.id.listview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                FigureOptions selectedOption = (FigureOptions) view.findViewById(R.id.row_name).getTag();
                Intent intent = new Intent(getApplicationContext(), selectedOption.cls);
                startActivity(intent);
            }
        });

        listView.setAdapter(new OptionsAdapter(figureOptions, getApplicationContext()));
    }
}
