package com.wdowiak.calculateareaof;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class OptionsAdapter extends ArrayAdapter
{
    private ArrayList<FigureOptions> data;
    Context context;

    // view lookup cache
    private static class ViewHolder
    {
        TextView figureNane;
    }

    public OptionsAdapter(ArrayList<FigureOptions> data, Context context)
    {
        super(context, R.layout.row, data);
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public FigureOptions getItem(int position)
    {
        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        View result;

        if(convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
            viewHolder.figureNane = convertView.findViewById(R.id.row_name);

            result = convertView;
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        FigureOptions figureOption = getItem(position);
        viewHolder.figureNane.setText(figureOption.name);

        viewHolder.figureNane.setTag(figureOption);
        return result;
    }
}
